'''
Created on May 31, 2013

@author: mivey
'''
from com.thelifeofageek.amiupv2 import upfile
from com.thelifeofageek.amiupv2.check import Check
from com.thelifeofageek.amiupv2.log import log
from com.thelifeofageek.amiupv2.mail import mail
import sys
import datetime


def amiuprun():
    #amiupmail = mail()
    
    # Set Logfile and prepend time stamp to each run
    amiuplog = log("/tmp/amiuplogv2.log")
    amiuplog.write_to_log_file("#" * 25)
    amiuplog.write_to_log_file(" "+ str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M')) +" ")
    amiuplog.write_to_log_file("#" * 25)
    amiuplog.write_to_log_file("\n")
    amiuperrlog = log("/tmp/amiuplogv2err.log")
    amiuperrlog.write_to_log_file("#" * 25)
    amiuperrlog.write_to_log_file(" "+ str(datetime.datetime.now().strftime('%Y-%m-%d %H:%M')) +" ")
    amiuperrlog.write_to_log_file("#" * 25)
    amiuperrlog.write_to_log_file("\n")
    
    # Get Sites from file and add to a list
    amiupfile = upfile.upfile()
    amiupfile.setFile("host.conf")
    amiupsites = amiupfile.getData()
    print amiupsites
    #count = None
    
    # iterate through list of sites to check status
    for siteline in amiupsites:
        #count = 1
        site, sitekey, sitekeytype = siteline
        print site
        print sitekey
        sitecodetype = str(sitekeytype).rstrip("\n")
        print sitecodetype
        checksite = Check()
        sitename = checksite.check_http(site)
        if sitecodetype == "d":
            sitecodereturn = checksite.check_site_code(sitename, sitekey)
            #print sitecodereturn
            amiuplog.write_to_log_file(sitecodereturn)
        elif sitecodetype == "s":
            sitetextreturn = checksite.check_site_text(sitename, sitekey)
            ##print sitetextreturn
            amiuplog.write_to_log_file(sitetextreturn)
        else:
            print "Need type for search"
            sys.exit(1)
        print sitename




if __name__ == "__main__":
    amiuprun()