'''
Created on May 31, 2013

@author: mivey
'''
from com.thelifeofageek.amiupv2 import check

class Site():
    def __init__(self):
        self.name = None
        self.code = None
        self.word = None
        self.filename = None
        self.cleanname = None

    def get_code(self):
        return self.code


    def get_word(self):
        return self.word


    def get_filename(self):
        return self.filename


    def get_cleanname(self):
        return self.cleanname


    def set_code(self, value):
        self.code = value


    def set_word(self, value):
        self.word = value


    def set_filename(self, value):
        self.filename = value


    def set_cleanname(self, value):
        self.cleanname = check.Check.clean_url(self, value)
        self.cleanname = value

        

    def get_name(self):
        return self.name


    def set_name(self, value):
        self.name = value
        
        
    
        