'''
Created on May 31, 2013

@author: mivey
'''

from datetime import datetime
from com.thelifeofageek.amiupv2 import upfile
import smtplib
import string
import getpass
import sys

class mail():
    """ Assigns the fileds to the right variables and creates layout for email"""
    def __init__(self):
        self.email = []
        self.SUBJECT = ""
        self.TO = ""
        self.FROM = ""
        self.text = ""
        self.SERVER = ""
        self.username = ""
        self.password = ""
        self.BODY = " "
        self.getEmailConf()
        
    def getEmailConf(self):
        mailConf = upfile.upfile()
        mailConf.setFile("email.conf")
        loadedmailconfig = mailConf.getEmail()
        print "The following was loaded from email config\n%s"% loadedmailconfig
        self.email = loadedmailconfig
                
             
    def setEmail(self, subject, text):
        print self.email
        self.SERVER, toadd, self.FROM = self.email
        self.TO = ",".join(toadd.split())
        self.SUBJECT = subject
        self.text = text
        currenttime = datetime.now().strftime('%Y-%m-%d %H:%M')
        msg = ("Subject: %s: %s\nFrom: %s\r\nSent To: %s\r\n\r\n" % (self.SUBJECT, str(currenttime), self.FROM, self.TO))
        self.BODY = msg + self.text
        
    def sendLocalMail(self):
        """ Sends email to person"""
        try:
            server = smtplib.SMTP(self.SERVER)
            server.sendmail(self.FROM, [self.TO], self.BODY)
            server.quit()
            print "Email sent"
        except:
            print "Failed to send email. Please check your settings"
        
    def sendWebMail(self):
        """ Sends email to person"""
        try:
            server = smtplib.SMTP(self.SERVER)
            server.starttls()  
            server.login(self.username,self.password)  
            server.sendmail(self.FROM, [self.TO], self.BODY)
            server.quit()
            print "Email sent"
        except:
            print "Failed to send email. Please check your settings"
        