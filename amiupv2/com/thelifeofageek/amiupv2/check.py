'''
Created on May 31, 2013

@author: mivey
'''
from com.thelifeofageek.amiupv2.log import log
from com.thelifeofageek.amiupv2 import mail
import urllib2
import os
import socket

class Check():
    def __init__(self):
        self.setLogging()
        self.setMailling()
        socket.setdefaulttimeout(240)
        
    def setMailling(self):
        self.amiupmail = mail.mail()
    
    def setLogging(self):
        self.amiuperrlog = log("/tmp/amiuplogv2err.log")
        
    def check_site_Down(self, sitename, message=""):
        try:
            file = open("/tmp/" + self.clean_url(sitename), "r")
            file.close()
            print "site was down but is now back up"
            self.amiuperrlog.write_to_log_file(sitename + " site was down but is now back up\n")
            os.remove("/tmp/" + self.clean_url(sitename))
            subject = "Alert: "+ sitename + " is back up."
            text = sitename + " is back up. Site Response below\n" + message
            self.amiupmail.setEmail(subject, text)
            self.amiupmail.sendLocalMail()
        except IOError:
            print "Site was not down"
    
    def set_site_Down(self, sitename, message=""):
        try: 
            file = open("/tmp/"+ self.clean_url(sitename), "r")
            file.close()
            print "site already marked down"
        except IOError:
            file = open("/tmp/" + self.clean_url(sitename), "w")
            file.write("Temp File to mark site as down")
            file.flush()
            file.close()
            print "site has been set to down in tmp folder"
            self.amiuperrlog.write_to_log_file(sitename +" site has been set to down in tmp folder\n")
            subject = "Warning: " + sitename + " is down."
            text = sitename + " is down. Please check your server. Site response below\n" + message
            self.amiupmail.setEmail(subject, text)
            self.amiupmail.sendLocalMail()
        
    def clean_url(self, sitename):
        if "https://" in sitename:
            sn = str(sitename[8:]).split("/",1)[0]
            #print sn
        elif "http://" in sitename:
            sn = str(sitename[7:]).split("/",1)[0]
            #print sn
#         if "/" in sitename:
#             head, sep, tail = sitename.partition('/')
#             sn = head
        return sn
    
    def check_site_code(self, sitename, sitecode):
        ''' Compare the site code to the code returned from the site'''
        #print type(sitecode)
        try:
            #socket.setdefaulttimeout(60)
            print "Timeout was set to %s"% (socket.getdefaulttimeout())
            checkCode = urllib2.urlopen(sitename)
            checkCode.getcode()
            siteText = checkCode.read()
            statusCode = int(checkCode.code)
        except IOError:
            self.amiuperrlog.write_to_log_file("Site %s Timed out\n"% sitename)
            siteText = "Time out issue"
            print "Site %s Timed out\n"% sitename ## Log this in the future
            statusCode = 0
        if int(statusCode) == 0:
            self.amiuperrlog.write_to_log_file("Site %s not reachable\n"% sitename)
            self.set_site_Down(sitename, siteText)
            return "Site %s not reachable\n"% sitename ## Log this in the future
        elif int(sitecode) == statusCode:
            self.check_site_Down(sitename)
            return "Site %s is up with %s\n"% (sitename, statusCode) ## Log this in the future
        else:
            #self.amiuperrlog.write_to_log_file("Site " + sitename +" Code was %s and Status Code was %s\n"% (sitecode, statusCode))
            self.amiuperrlog.write_to_log_file("Site %s is down with %s\n"% (sitename, statusCode))
            self.set_site_Down(sitename, siteText)
            print "Site Code was %s and Status Code was %s\n"% (sitecode, statusCode) # debug info
            return "Site %s is down without %s\n"% (sitename, statusCode) ## Log this in the future
    
    
    def check_site_text(self, sitename, sitetext):
        ''' Compare the text to the text in the HTML page '''
        try:
            #socket.setdefaulttimeout(60)
            print "Timeout was set to %s"% (socket.getdefaulttimeout())
            checkText = urllib2.urlopen(sitename)
            checkTdata = checkText.read()
        except IOError:
            self.amiuperrlog.write_to_log_file("Site %s Timed out\n"% sitename)
            print "Site %s Timed out\n"% sitename ## Log this in the future
            checkTdata = None
        if checkTdata == None:
            self.amiuperrlog.write_to_log_file("Site %s not reachable\n"% sitename)
            self.set_site_Down(sitename)
            return "Site %s not reachable\n"% sitename ## Log this in the future
        elif sitetext in checkTdata: 
            self.check_site_Down(sitename, checkTdata)
            return "Site %s is up with %s\n"% (sitename, sitetext) ## Log this in the future
        else:
            self.amiuperrlog.write_to_log_file("Site %s is down without %s\n"% (sitename, sitetext))
            self.set_site_Down(sitename, checkTdata)
            return "Site %s is down without %s\n"% (sitename, sitetext) ## Log this in the future
    
    def check_http(self, sitename):
        ''' Check that http or https is prepended to the domain name '''
        if "http" in sitename:
            print "Site %s had http\n"% sitename
            return sitename
        else:
            print "Site  %s did not have http\n"% sitename 
            return "http://" + sitename
