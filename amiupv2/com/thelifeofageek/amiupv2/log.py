'''
Created on May 31, 2013

@author: mivey
'''

class log():
    def __init__(self, lfname):
        self.set_log_file(lfname)
        self.logFile = self.get_log_file()
        self.check_log_file_exist()

    def get_log_file(self):
        return self.__logFile


    def set_log_file(self, value):
        self.__logFile = value


    def del_log_file(self):
        del self.__logFile
        
    def check_log_file_exist(self):
        try:
            with open(self.logFile): pass
            print "File exist"
        except IOError:
            print 'File Does not exist'
            print 'Creating file'
            self.create_log_file()
        
    def create_log_file(self):
        file = open(self.logFile, "w")
        file.write("### Log File for amiupv2 ###")
        file.flush()
        file.close()
        
    def write_to_log_file(self, logThis):
        file = open(self.logFile, "a")
        file.write(logThis)
        file.flush()
        file.close()

    
    logFile = property(get_log_file, set_log_file, del_log_file, "logFile's docstring")


