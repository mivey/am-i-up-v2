'''
Created on May 31, 2013

@author: mivey
'''
import sys

class upfile():
    def __init__(self):
        self.lines = []
        self.email = []
        
    def setFile(self, fname):
        try:
            file = open(fname, "r")
        except IOError:
            print "No file named "+ fname
            print "Creating " + fname + " file"
            file = open(fname, "w")
            file.close()
            print "file created"
            sys.exit(1)
        for line in file.readlines():
            if '#' in line:
                pass
            elif '=' in line:
                print line.split("=")
                #self.email.append(line.split("=")[1])
                self.setEmail(str(line.split("=")[1]).rstrip("\n"))
            else:
                #self.hosts.append(host.split(","))
                self.setData(line.split(","))
    
    def setData(self, host):
        self.lines.append(host)
        
    def getData(self):
        return self.lines
    
    def setEmail(self, line):
        self.email.append(line)
        
    def getEmail(self):
        return self.email