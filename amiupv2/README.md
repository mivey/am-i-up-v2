amiupv2 Readme File

Amiupv2 script will let you check to see if a website is online by either the return code, or text in the page. It can alert you via email if a site goes down or is unreachable, and when it comes back online. It can also log the up and down's of a site to a log file for future review.

2 config files are needed to run this script. both files are created the first time you run the script. You will have to populate them with the correct information.


*host.conf
	## example ##
	## https://www.yoursite.com,200,d 
	## or
	## https://www.yoursite.com,sitetitle,s
	##
	## You can either look for the return code of the site or look for text in the page
	## that should always be up.
	##

*email.conf
	# format
	# server=127.0.0.1
	# emailto=youremail@yourdomain.com
	# emailfrom=no-reply@yourdomain.com
	server=
	emailto=
	emailfrom=

Also this program can be scheduled to run using either cron or windows Task scheduler.

example cron:

   # m h  dom mon dow   command
   */2 * * * * /home/yourname/amiupv2/amiupv2.py ## This example has the script running every 2 minutes. 
